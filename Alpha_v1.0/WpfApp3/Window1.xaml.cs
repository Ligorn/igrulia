﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WpfApp3
{
    public partial class Window1 : Window
    {
        int[,] game = new int[4, 4];
        int count;
        int sec;
        int empty_x, empty_y;
        static Random rand = new Random();

        public Window1()
        {
            InitializeComponent();
            Start();
            for (int i = 0; i < 50; i++)
                move_random();
                count = 0;
            count_label.Content = count.ToString();
            Update();
        }

        

        private void num_Clicked(object sender, RoutedEventArgs e)
        {
            int pos = Convert.ToInt16(((Button)sender).Uid);
            move(pos);
            Update();
            if (GameOver())
            {
                MessageBox.Show("Вы победили за " + count + " шагов. Нажмите 'ОК', чтобы выйти в меню.", "ПОБЕДА!");
                this.Close();
                MainWindow mainMenu = new MainWindow();
                mainMenu.Show();
            }

        }

        private void move(int pos)//Передвижение
        {
            int x, y;
            GetCoords(pos, out x, out y);
            if (Math.Abs(empty_x - x) + Math.Abs(empty_y - y) != 1)
                return;
            count++;
            count_label.Content = count.ToString();
            game[empty_x, empty_y] = game[x, y];
            game[x, y] = 0;
            empty_x = x;
            empty_y = y;
        }

        private bool GameOver()//
        {
            if (!(empty_x == 3 && empty_y == 3))
                return false;
            for (int x = 0; x < 4; x++)
                for (int y = 0; y < 4; y++)
                    if (!(x == 3 && y == 3))
                        if (game[x, y] != GetPosition(x, y) + 1)
                            return false;
            return true;
        }

        private void move_random()//Перемешивание
        {
            int a = rand.Next(0, 4);
            int x = empty_x;
            int y = empty_y;
            switch (a)
            {
                case 0:
                    x--;
                    break;
                case 1:
                    x++;
                    break;
                case 2:
                    y--;
                    break;
                case 3:
                    y++;
                    break;
            }
            move(GetPosition(x, y));
        }
        private void Update()//Обновление поля
        {
            for (int pos = 0; pos < 16; pos++)
            {
                button(pos).Content = GetNumber(pos).ToString();
                if (GetNumber(pos) == 0)
                {
                    button(pos).Visibility = Visibility.Hidden;
                }
                else
                    button(pos).Visibility = Visibility.Visible;
            }

        }

        private void Start()
        {
            for (int x = 0; x < 4; x++)
                for (int y = 0; y < 4; y++)
                    game[x, y] = GetPosition(x, y) + 1;
            empty_x = 3;
            empty_y = 3;
            game[empty_x, empty_y] = 0;
        }

        private int GetNumber(int pos)//Получение номера кнопки по позиции
        {
            int x, y;
            GetCoords(pos, out x, out y);
            return game[x, y];
        }

        private int GetPosition(int x, int y)//Получение позиции по кооржинатам
        {
            if (x < 0)
                x = 0;
            if (x > 3)
                x = 3;
            if (y < 0)
                y = 0;
            if (y > 3)
                y = 3;
            return y * 4 + x;
        }

        private void GetCoords(int pos, out int x, out int y)//Получение координат по позиции
        {
            if (pos < 0)
                pos = 0;
            if (pos > 15)
                pos = 15;
            x = pos % 4;
            y = pos / 4;
        }

        private void specksWindow_Closed(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private Button button(int position)//
        {
            switch (position)
            {
                case 0: return num0;
                case 1: return num1;
                case 2: return num2;
                case 3: return num3;
                case 4: return num4;
                case 5: return num5;
                case 6: return num6;
                case 7: return num7;
                case 8: return num8;
                case 9: return num9;
                case 10: return num10;
                case 11: return num11;
                case 12: return num12;
                case 13: return num13;
                case 14: return num14;
                case 15: return num15;
                default: return null;
            }
        }
    }
}
