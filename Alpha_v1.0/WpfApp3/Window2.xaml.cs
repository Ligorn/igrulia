﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp3
{
    /// <summary>
    /// Логика взаимодействия для Window2.xaml
    /// </summary>
    public partial class Window2 : Window
    {
        String tmpf = "";
        String temps = "";
        bool plus = false;
        bool minus = false;
        bool mult = false;
        bool div = false;
        double first = 0;
        double second = 0;

        public Window2()
        {
            InitializeComponent();
        }

        private void workSpace_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.OemPlus)
            {
                plus = true;
                first = Convert.ToDouble(workSpace.Text);
            }
            else if (e.Key == Key.OemMinus)
            {
                minus = true;
                first = Convert.ToDouble(workSpace.Text);
            }
            else if (e.Key == Key.Multiply)
            {
                mult = true;
                first = Convert.ToDouble(workSpace.Text);
            }
            else if (e.Key == Key.Divide)
            {
                div = true;
                first = Convert.ToDouble(workSpace.Text);
            }

            if (e.Key == Key.Enter)
            {
                resultNum.Content = "";
                tmpf = workSpace.Text;
                for (int i = 1; i < tmpf.Length; i++)
                    if (tmpf[i - 1] == '+' || tmpf[i - 1] == '-' || tmpf[i - 1] == '*' || tmpf[i - 1] == '/')
                        temps += tmpf[i];
                second = Convert.ToDouble(temps);
                if (plus == true)
                {
                    //first = first + second;
                    resultNum.Content = first + second;
                }
                else if (minus == true)
                {
                    //first = first - second;
                    resultNum.Content = first - second;
                }
                else if (mult == true)
                {
                    //first = first * second;
                    resultNum.Content = first * second;
                }
                else if (div == true)
                {
                    //first = first / second;
                    resultNum.Content = first / second;
                }
                temps = "";
                plus = false;
                minus = false;
                mult = false;
                div = false;
                workSpace.Text = "";
            }
        }

        private void calcWindow_Closed(object sender, EventArgs e)
        {
            Environment.Exit(0);
            /*MainWindow menu = new MainWindow();
            menu.Show();
            this.Close();*/
        }
    }
}
