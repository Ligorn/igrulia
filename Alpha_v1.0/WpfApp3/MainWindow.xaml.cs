﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp3
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void buttonExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void buttonSpecks_Click(object sender, RoutedEventArgs e)
        {
            Window1 specks = new Window1();
            specks.Show();
            this.Hide();
        }

        private void buttonCalc_Click(object sender, RoutedEventArgs e)
        {
            Window2 calc = new Window2();
            calc.Show();
            this.Hide();
        }
    }
}
